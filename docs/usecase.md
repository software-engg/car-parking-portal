# ONLINE CAR PARKING GARAGE PORTAL

## **USE CASE DIAGRAM**

![USECASE (3).jpeg](media/usecase.jpeg)

  ***VI : Vehicle Identification***  
  ***VIN : Vehicle Identification Number***

**DESCRIPTION OF USE-CASES**

1. **Register an Account:** Customer can create an account
    with their following information: name, address, contact details
    etc. 

2.  **Authentication:** To authenticate user and separate
    them on basis of their privilege. Admin can add new data to the system.

2.  **Garage Information:** The portal provides
    information regarding the location, services offered and procedures
    followed in the car parking garage. It also provides information on
    types of vehicles allowed and the capacity of the garage.

4.  **Vehicle Identification:** The vehicle license plate
    is noted along with registration number and vehicle type and submits
    this information to Vehicle Identification System.

5.  **Manage Database:** Admin will manage the database
    system to keep records of bookings, cancellations, customer
    information (including vehicle info), block spam users, maintain a
    list of important customers (including VIPs, emergency services
    etc.)

6.  **Book Parking Space:** Reserve a parking space in
    garage through online portal and confirm parking spot with adequate
    duration of stay. User can also reschedule his/her booking.

7.  **Cancel Booking:** Cancel the booked space through
    portal.

8.  **Pay Bills:** After confirmation of the reservation,
    proceed to different modes of payment.

9.  **Analytics:** The portal will implement APIs
    (Application Programming Interface) to keep records of the customers
    and formulates a usage analytics to show a customer-pattern.

10.  **Help & Support:** An enquiry option in the portal to
    contact technical assistants through mail or phone service regarding
    the parking garage and security issues. Customer can also request
    for emergency parking services and VIP bookings.

**ASSUMPTIONS:**

1.  Customer has some experience using an Online Portal.

2.  The customer has necessary requirements to make online payments for
    bills.

3.  Information provided by the user must be accurate. Forged id's and
    fake addresses are not accepted.

4.  The portal has been thoroughly tested and all personal data is kept
    safe and secure.

5.  Every customer's car is parked in the respective parking lot.

6.  The vehicle recognition devices can accurately verify license plate
    numbers even if there are minor dents and scratches on the plate.

7.  Users will not forcefully enter the parking lot if his/her
    registration is cancelled or her/his term has expired.

8.  Any change in information provided by the customer is updated in the
    database upon receiving the new information to avoid errors in the
    future.

9.  Database system will be managed by the Administrator.
