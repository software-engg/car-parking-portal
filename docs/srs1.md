<p align="center">
    <img
      alt="CC 4.0"
      src='./88x31.png'
      width="100"
    />
</p>
This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.  


# Software Requirements Specification
<h3 align="center">ONLINE CAR PARKING GARAGE PORTAL</h3>

<!--Version 0.1.2 approved-->  
Prepared by  **Vipul Kumar**, **Srayan Ray**, **Debabrata Mukherjee**, **Saurav Kumar**.

Software Engineering Assignment

Sun Sep 24<sup>th</sup> 2018

## Table of Contents

<h4 align="center">Table of Contents</h4>   

<!-- Revision History -->
1.	Introduction   
    1.1	Purpose	  
    1.2	Document Conventions	  
    1.3	Intended Audience and Reading Suggestions	  
    1.4	Product Scope	  
    1.5	References	  
2.	Overall Description	   
    2.1	Product Perspective	  
    2.2	Product Functions	  
    2.3	Operating Environment	  
    2.4	Design and Implementation Constraints	  
    2.5	User Documentation	  
    2.6	Assumptions and Dependencies	  
3.	External Interface Requirements	  
    3.1	User Interfaces	  
    3.2	Hardware Interfaces	  
    3.3	Software Interfaces	  
    3.4	Communications Interfaces	  
4.	Functional Requirements	  
    4.1 Register an account 	  
    4.2 Authentication	  
    4.3. Garage information    
    4.5 vehicle identification    
    4.6 Book parking space  
    4.7 Bill payments  
    4.8. Cancel booking  
    4.9 Analytics  
    4.10 Manage database  
    4.11 Help and support  
5.	Other Nonfunctional Requirements	  
    5.1	Performance Requirements	  
    5.2	Safety Requirements	  
    5.3	Security Requirements	  
    5.4  Usability   
    5.5 Reliability & Availability  
    5.6 Supportability  
    5.7 Design Constraints  
    5.8 On-line User Documentation and Help System Requirements   
6.	Other Requirements    
    6.1 Licensing Requirements  
    6.2 Legal, Copyright, and Other Notices  
    6.3 Applicable Standards    

Appendix A: Glossary	  
Appendix B: Analysis Models	  
Appendix C: To Be Determined List	  

## Revision History

  ------ ------ -------------------- ---------
  | Name | Date | Reason For Changes | Version |
  | ---- | :--: | :----------------: | :-----: |
  | SRS1 | 17<sup>th</sup> sep 2018 | Initial commit | 0.1.0
  | SRS1 | 24<sup>th</sup> sep 2018 | Fix typo errors | 0.1.1
  | SRS1 | 10<sup>th</sup> dec 2018 | add license | 0.1.2


  
## 1. Introduction

  ### 1.1 Purpose   
The purpose of this document is to collect and analyze all assorted ideas that have come up to define the system, its requirements with respect to consumers. Also, we will predict and sort out how we hope this product will be used in order to gain a better understanding of the project, outline concepts that may be developed later, and document ideas that are being considered, but may be discarded as the product develops.   
In short, the purpose of this SRS document is to provide a detailed overview of our software product, its parameters and goals. This document describes the project's target audience and its user interface, hardware and software requirements. It defines how our client, team and audience see the product and its functionality. Nonetheless, it helps any designer and developer to assist in software delivery lifecycle (SDLC) processes.  

### 1.2 Document Conventions  
#### 1.2.1 Typographical convention :  
1. Headings (Times New Roman, 18/14)
2. Text (Arial, 11)

#### 1.3 Intended Audience and Reading Suggestions   
  * Software maintainers, developers or contributors.

#### 1.4 Product Scope   
Product is intended for private and commercial vehicle parking. Dealers and other organizations can also use the space to extend services.
This software helps those people who wants to book parking space online. As well as some parking space dealers to use portal and reach to more
people.

#### 1.5 References   
  * Wiki page of a project [Car Parking Portal](https://gitlab.com/software-engg/car-parking-portal/wikis/home)
  * [Use-Case Diagram](./usecase.md)
  * Data Flow Diagram
      * [Level 1](./media/dfd-l1.jpeg)
      * [Level 2](./media/dfd-l2.jpeg)
  * [ER Diagram](./media/er.jpeg)

## 2.  Overall Description

### 2.1 Product Perspective

This document contains the problem statement that the current system is
facing which is hampering the growth opportunities of the company. It
further contains a list of the stakeholders and users of the proposed
solution. It also illustrates the needs and wants of the stakeholders
that were identified in the brainstorming exercise as part of the
requirements workshop. It further lists and briefly describes the major
features and a brief description of each of the proposed system.

The following SRS contains the detail product perspective from different
stakeholders. It provides the detail product functions of Online Parking
Portal with user characteristics permitted constraints, assumptions and
dependencies and requirements subsets.

### 2.2 Product Functions   
1. Register an account  
2. Authentication  
3. Garage information    
4. Vehicle Identification   
5. Manage Database  
6. Book parking space    
7. Cancel Booking   
8. Pay Bills  
9. Analytics  
10. Help & support  

### 2.3 Operating Environment   
* The operation of the portal is platform independent and requires a modern web-browser (Chrome, Firefox etc).

### 2.4 Design and Implementation Constraints  
1. Realtime Database  
    a). Maximum Simultaneous connections : 100  
    b). Maximum Storage Capacity : 1GB  
2. Maximum Storage : 5GB  
3. Language Requirements : Javascript, PHP, SQL.   

### 2.5 User Documentation  
As the product is parking portal, Online help system becomes a critical component of the system which will provide -   
  * It will provide specific guidelines to a user for using the portal and within the system.   
  * To implement online user help, link and search fields will be provided.

### 2.6 Assumptions and Dependencies   

  * This a web based application which uses
[000webhost](https://www.000webhost.com/) for application
hosting and its development.

## 3.  External Interface Requirements

### 3.1 User Interfaces
  * The user interface of the software will be compatible with any modern browser (such as Chrome, Firefox, Brave etc) by which user can access to the system.

### 3.2 Hardware Interfaces   
* Since the application is run over the internet, all the hardware shall
require to connect internet will be hardware interface for the system. As for e.g. Modem, WAN -- LAN, Ethernets.

### 3.3 Software Interfaces
1.  The system will communicate with the content manager to get the current status of garage, its specifications, offerings and promotions.  
2.  The system will communicate with bill Pay system to identify available payment methods , validate the payments and process payment requests.  
3.  The system will communicate to credit management system for handling financing options.  
4.  The system will communicate with CRM system to provide help & support.  
5.  The system shall communicate with VI for order management.
6.  The system will communicate with queue management system for tracking booking status and updating queue.
7.  The system will communicate with external Tax system to calculate GST.
8.  The system will communicate over TLS/SSL and PKI which allows user to complete secure transactions.

### 3.4. Communications Interfaces  
* The parking portal system will use the HTTP(S) protocol for
communication over the internet and for the intranet communication will be through TCP/IP protocol suite.

## 4. Functional Requirements  

### 4.1 Introduction   

* This subsection contains the functional requirements for the portal.
These requirements are organized by the features discussed in the
Use-Case diagram to capture the functional requirements of the system.

#### 4.1.1 Register an account 
* The system will allow user to create profile and set his/her credential.
* The system will maintain customer's email information as a required part of customer profile.

#### 4.1.2 Authentication   
* The system will authenticate user credentials to view the profile and categories them on basis of there privilege and role.
* The system will allow user to update the profile information.
* The system will display both the active and completed booking history in
the customer profile.
* The system will allow user to select the booking ID from the order history.
* The system will display the detailed information about the selected booking.
* The system will display the most frequently searched locations and parking space by the user in the profile.

#### 4.1.3 Garage information   
* The system will allow user to enter the search text on the search bar.
* The system will allow user to select multiple options on the screen to search.
* The system will display all the matching parking space based on the search.
* The system will display at most 10 matching result on the current screen.
* The system will notify the user when no matching product is found on the search.
* The system will allow user to navigate between the search results.

#### 4.1.4 vehicle identification
* The system will send vehicle meta-data to VI system.
* The system will verify vehicle information with returned data of VI system.

#### 4.1.6 Book parking space
* The system will send an booking confirmation to the user through email.
* The system shall allow user to select available promotions.

#### 4.1.7 Bill payments
* The system will display available payment methods for payment.
* The system will display the booking charges for different payment method.
* The system will allow user to select the payment method for booking.
* The system will allow user to confirm the booking.
* The system will redirect to corresponding payment portal on the basis of payment method.
* The system will display detailed invoice for current booking once it is confirmed.
* The system will optionally allow user to print the invoice.

#### 4.1.8 Cancel booking

* The system will allow user to select the booking to be changed.
* The system will allow user to cancel the booking
* The system will notify the user about any changes made to the booking through email.

#### 4.1.9 Analytics

* The system will show performance metrics, real-time crash reports to admin.
* The system will monitor user's behavior and target them advertisement.
* The system will customize the search result on basis of user's meta-data.
* The system will display the reviews and ratings of each parking space, when it is selected.
* The system will enable the user to enter their reviews and ratings.
* The system will display all the available promotions to the user.    

#### 4.1.10 Manage database    

* The system will allow admin to create, remove new databases
* The system will allow admin to block spam users.

#### 4.1.10 Help and support   

* The system will provide online help, FAQ's customer support, and sitemap options for customer support.
* The system will allow user to select the support type he wants.
* The system will allow user to enter the customer and type of information for the support.
* The system will display the customer support contact numbers on the screen.
* The system will allow user to enter the contact number for personnel to call support.
* The system will display the online help upon request.
* The system will display the FAQ's upon request.
* The system will allow user to view detailed sitemap.

## 5.  Other Nonfunctional Requirements

### 5.1 Performance Requirements

* The product will be based on web and for hosting and development purpose it will use [Firebase](https://firebase.google.com/) platform.

* The product will take initial load time depending on internet connection
strength which also depends on the media from which the product is run.

* The performance will depend upon hardware components of the
client/customer.

### 5.2 Safety Requirements

* VI system will work properly
* At the time system's maintenance or failure software will use backup system.

### 5.3 Security Requirements

#### 5.3.1 Data Transfer

* The system will use secure sockets in all transactions that include any confidential customer information.
* The system will automatically log out all customers after a certain period of inactivity.
* The system will confirm all transactions with the user's web browser.
* The system will not leave any cookies on the user's computer containing the user's password.
* The system will not leave any cookies on the user's computer containing any of the user's confidential information.

#### 5.3.2 Data Storage

* The user's web browser will never display password. It will always be echoed with special characters representing typed characters.
* The customer's web browser will never display a customer's credit card number after retrieving from the database. It will always be shown with just the last 4 digits of the credit card number.
* The system's back-end servers will only be accessible to authenticated administrators.
* The system's back-end databases will be encrypted using modern encryption technique.

### 5.4 Usability

#### 5.4.1 Graphical User Interface

* The system will provide a uniform look and feel between all the web
pages.

### 5.4.2 Accessibility

* The system will provide multi language support.
* The system will provide multiple theme support.  

### 5.5 Reliability & Availability 

#### 5.5.1 Back-end

* The system will provide storage of all databases on redundant computers with automatic switchover.
* The system will provide for replication of databases to off-site storage locations.
* The system will provide RAID 5 Disk Stripping on all database storage disks.

#### 5.5.2 Internet Service Provider

* The system will provide a contractual agreement with an internet service
provider for T3 access with 99.9999% availability.
*  The system will provide a
contractual agreement with an internet service provider who can provide 99.999% availability through their network facilities onto the internet.

### 5.6 Supportability

#### 5.6.1 Configuration Management Tool

* The source code developed for this system will be maintained in configuration management tool.

### 5.7 Design Constraints  

#### 5.7.1 Standard Development Tools   

* The system will be built using a standard web page development tools.

#### 5.7.2 Web Based Product  

* There are no memory requirements
* The computers must be equipped with a modern web browsers.
* Response time for loading shouldn't take longer than five minutes.

#### 5.8 On-line User Documentation and Help System Requirements  

As the product is parking portal, On-line help system becomes a critical component of the system which will provide : 

  * It will provide specific guidelines to a user for using the portal.
  * To implement online user help and support, link and search fields will be provided.

### 6  Other Requirements   

#### 6.1 Licensing Requirements

[CC 4.0](http://creativecommons.org/licenses/by-nc-sa/4.0/) and [GPL v3.0](../LICENSE)

#### 6.2 Legal, Copyright, and Other Notices   

Portal should display the disclaimers, copyright, word mark, cookies and privacy policy.

#### 6.3 Applicable Standards   

It should be as per the industrial standard.

## Appendix A : Definations   

Configuration : It means a product which is available / Selected from a catalogue can be customized.

## Appendix B : Glossary  

* FAQ : Frequently asked questions
* CRM : Customer relationship management
* VI : Vehicle identification
* VIN : Vehicle identification number
* HTTP : Hypertext transfer protocol
* SSL : Secure socket layer
* TLS : Transport layer security
* PKI : Public key infrastructure
* GST : Goods sell tax
* RAID5 : Redundant Array of Inexpensive Disk/Drives  

![cc 4.0][1]  
This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.  

[1]:./88x31.png
