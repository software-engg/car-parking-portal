# Online Car Parking Garage Portal
  * [Problem Statement](./docs/pstatement.md)
  * [Use Case Diagram](./docs/usecase.md)
  * Data Flow Diagram
      * [Level 1](./docs/media/dfd-l1.jpeg)
      * [Level 2](./docs/media/dfd-l2.jpeg)
  * ER Diagram
      * [gliffy](./docs/media/er2.gliffy)
      * [jpeg](./docs/media/er2.jpeg)
  * Software Requirements Specification [md](./docs/srs1.md) or [pdf](./docs/srs1.pdf)
  * [Data Dictionary](./docs/data_dict.pdf)
# UML
  * [Quick-reference](http://www.mcdonaldland.info/files/designpatterns/designpatternscard.pdf)
  * [Cheat-Sheet](https://www.slideshare.net/hustwj/design-patterns-cheat-sheet)

  
# License
  * Except as otherwise noted, the content of this project is licensed under a [GPL v3.0](./LICENSE)

# Automation   
  - [Set Up Continuous Integration Pipelines with GitLab CI](https://www.digitalocean.com/community/tutorials/how-to-set-up-continuous-integration-pipelines-with-gitlab-ci-on-ubuntu-16-04) - `DigitalOcean`  
  - [Getting started with GitLab CI/CD](https://docs.gitlab.com/ce/ci/quick_start/README.html)
  - [Configuration of your jobs with .gitlab-ci.yml](https://docs.gitlab.com/ce/ci/yaml/README.html)   


**License Compatibility**  
  * [Choose a License](https://choosealicense.com/licenses/)  
  * [Software Licenses](https://tldrlegal.com/) - `tl;drlegal`  
  * [GPL compatible Licenses](https://www.gnu.org/licenses/license-list.html#GPLCompatibleLicenses)
  * [License_compatibility](https://en.wikipedia.org/wiki/License_compatibility) - `Wikipedia`  
  * [Licensing](http://linux-training.be/linuxfun.pdf) - `linuxfun.pdf`  
  * [floss-license-slide](https://www.dwheeler.com/essays/floss-license-slide.html) - `D wheeler`  
  * [licenses](https://janelia-flyem.github.io/licenses.html) - `Janelia Flyem`  
    
  

